# Portfolio tuto

Il s'agit d'un site internet présentant des peintures

## Environnement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose
* nodejs et npm/yarn

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony) :

```bash
symfony check:requirements
```
### Lancer l'environnement de développement

```bash
composer install
npm install
npm run build
docker-compose up -d
symfony serve -d
```

### Charger les fixtures (option)

```
symfony console doctrine:fixtures:load
```

### Lancer des tests

```bash
php bin/phpunit --testdox
```

Pour connaître la couverture de code actuel :

```bash
php bin/phpunit --coverage-html var/log/test/test-coverage
```

### Tests fonctionnels

* Créer une base de données pour l'environnement de test
```bash
APP_ENV=test symfony console doctrine:database:create
```
* Lancer les migrations

```bash
APP_ENV=test symfony console doctrine:migrations:migrate -n
```
* Lancer les fixtures pour avoir des données de test

```bash
APP_ENV=test symfony console doctrine:fixtures:load -n
```

### Accéder à la base de données Dockerisée

Vous pouvez utiliser l'extension **Docker** de VSCode :

* Step 1 - Accès à la BDD : clic droit sur le conteneur correspondant et sélectionner **Attach Shell**
* Step 2 - Connexion : Renseigner le mot de passe (docker-compose.yaml)
```bash
mysql -p
```
* Step 3 - Accéder à la base correspondante :
```bash
use (base)
```
* Step 4 - Lancer votre requête

### Solutions techniques appliquées dans le cadre du projet

* [Création d'une extension twig](./docs/extension_twig.md)

## Production

### Envoi des mails de Contacts

Les mails de prise de contact sont stockés en BDD, pour les envoyer au peintre par mail, il faut les mettre en place un cron sur :

```bash
symfony console app:send:contact
```