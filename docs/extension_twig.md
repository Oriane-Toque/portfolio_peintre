# Création d'une extension twig

* Documentation utilisée : [How to Write a custom Twig Extension](https://symfony.com/doc/current/templating/twig_extension.html)

## Cas d'utilisations dans ce projet

* Pour **appeler toutes les catégories** (le nom seulement) dans toutes les vues afin d'afficher les résultats dans un menu.
    * Pour éviter des répétitions de codes dans tous les controleurs **DRY**
* Pour récupérer le nom de la route précédente via l'adresse de référence afin de créer un bouton de retour.