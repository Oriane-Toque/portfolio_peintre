<?php

namespace App\DataFixtures;

use App\DataFixtures\CategoryFixtures as CatRef;
use App\DataFixtures\UserFixtures as UserRef;
use App\Entity\Paint;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

/**
 * @codeCoverageIgnore
 */
class PaintFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i = 1; $i <= CatRef::NBR_CATEGORY; ++$i) {
            // Création de deux peintures par catégorie
            for ($j = 0; $j < 2; ++$j) {
                $paint = new Paint();

                $paint->setName($faker->words(3, true))
                      ->setWidth($faker->randomFloat(2, 20, 60))
                      ->setHeight($faker->randomFloat(2, 20, 60))
                      ->setOnSale($faker->randomElement([true, false]))
                      ->setRealisationDate($faker->dateTimeBetween('-6 month', 'now'))
                      ->setCreatedAt(new DateTime())
                      ->setDescription($faker->text())
                      ->setPortfolio($faker->randomElement([true, false]))
                      ->setSlug($faker->slug())
                      ->setFile('build/img/portfolio/paint1.jpg')
                      ->setPrice($faker->randomFloat(2, 100, 9999))
                      ->setUser($this->getReference(UserRef::USER_REFERENCE))
                      ->addCategorie($this->getReference(CatRef::CATEGORY_REFERENCE . $i));

                $manager->persist($paint);
            }
        }

        // Peinture pour le test
        $paint = new Paint();

        $paint->setName('Paint test')
              ->setWidth($faker->randomFloat(2, 20, 60))
              ->setHeight($faker->randomFloat(2, 20, 60))
              ->setOnSale($faker->randomElement([true, false]))
              ->setRealisationDate($faker->dateTimeBetween('-6 month', 'now'))
              ->setCreatedAt(new DateTime())
              ->setDescription($faker->text())
              ->setPortfolio($faker->randomElement([true, false]))
              ->setSlug('paint-test')
              ->setFile('build/img/portfolio/paint1.jpg')
              ->setPrice($faker->randomFloat(2, 100, 9999))
              ->setUser($this->getReference(UserRef::USER_REFERENCE))
              ->addCategorie($this->getReference(CatRef::CATEGORY_REFERENCE . 'test'));

        $manager->persist($paint);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CategoryFixtures::class,
        ];
    }
}
