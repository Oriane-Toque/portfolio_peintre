<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @codeCoverageIgnore
 */
class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    // user reference
    public const USER_REFERENCE = 'user';

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        // Utilisation de Faker
        // Ref : https://fakerphp.github.io/
        $faker = Factory::create('fr_FR');

        // Création d'un utilisateur
        $user = new User();
        $user->setEmail('user@test.com')
             ->setFirstname($faker->firstName())
             ->setLastname($faker->lastName())
             ->setTel($faker->phoneNumber())
             ->setAbout($faker->text())
             ->setRoles(['ROLE_PAINTER'])
             ->setInstagram('instagram');
        // Mots de passe hasher
        $user->setPassword($this->hasher->hashPassword($user, 'password'));

        $manager->persist($user);
        // Pour accéder à l'objet dans d'autres fixtures
        $this->addReference(self::USER_REFERENCE, $user);

        $manager->flush();

        return;
    }
}
