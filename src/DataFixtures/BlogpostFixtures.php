<?php

namespace App\DataFixtures;

use App\DataFixtures\UserFixtures as UserRef;
use App\Entity\Blogpost;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

/**
 * @codeCoverageIgnore
 */
class BlogpostFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        // Création objet faker
        $faker = Factory::create('fr_FR');

        // Récupération de l'objet User
        $user = $this->getReference(UserRef::USER_REFERENCE);

        // Création de 10 blogposts
        for ($i = 0; $i < 10; ++$i) {
            $blogpost = new Blogpost();
            $blogpost->setTitle($faker->words(3, true))
                     ->setContent($faker->text(350))
                     ->setSlug($faker->slug(3))
                     ->setCreatedAt(new DateTime())
                     ->setUser($user);

            $manager->persist($blogpost);
        }

        // Blogpost pour le test
        $blogpost = new Blogpost();

        $blogpost->setTitle('Blogpost test')
                 ->setCreatedAt(new DateTime())
                 ->setContent($faker->text(350))
                 ->setSlug('blogpost-test')
                 ->setUser($user);

        $manager->persist($blogpost);

        $manager->flush();

        return;
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
