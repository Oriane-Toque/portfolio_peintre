<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

/**
 * @codeCoverageIgnore
 */
class CategoryFixtures extends Fixture
{
    public const CATEGORY_REFERENCE = 'category_';
    public const NBR_CATEGORY = 6;

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i = 1; $i <= 6; ++$i) {
            $category = new Categorie();

            $category->setName($faker->word())
                     ->setDescription($faker->text())
                     ->setSlug($faker->slug());

            $manager->persist($category);

            $this->addReference(self::CATEGORY_REFERENCE . $i, $category);
        }

        // Catégorie pour le test
        $category = new Categorie();

        $category->setName('Category test')
                 ->setDescription($faker->text())
                 ->setSlug('category-test');

        $manager->persist($category);

        $this->addReference(self::CATEGORY_REFERENCE . 'test', $category);

        $manager->flush();

        return;
    }
}
