<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Paint;
use App\Repository\CategorieRepository;
use App\Repository\PaintRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PaintController extends AbstractController
{
    /**
     * Liste toutes les réalisations du peintre
     * @Route("/realisations", name="app_realisations", methods={"GET"})
     *
     * @param PaintRepository $pr pour récupérer les peintures
     * @param PaginatorInterface $paginator pour configurer la pagination
     * @param Request $request pour récupérer le numéro de la page
     * @return Response
     */
    public function realisations(
        PaintRepository $pr,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        // Récupération toutes les peintures
        $data = $pr->findAll();

        // Je configure ma pagination
        $paints = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );

        // Je configure le rendu de la barre pagination
        $paints->setCustomParameters([
            'align' => 'center',
            'size' => 'large',
            'rounded' => true,
        ]);

        return $this->render('paint/realisations.html.twig', [
            'paints' => $paints,
        ]);
    }

    /**
     * Portfolio du peintre
     * @Route("/portfolio", name="app_portfolio", methods={"GET"})
     *
     * @param CategorieRepository $cr pour récupérer toutes les catégories
     * @return Response
     */
    public function portfolio(CategorieRepository $cr): Response
    {
        $categories = $cr->findAll();

        return $this->render('paint/portfolio.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * Portfolio - liste les oeuvres du peintre selon la catégorie
     * @Route("/portfolio/{slug}",
     * name="app_portfolio_categorie",
     * methods={"GET"},
     * requirements={"slug"="[a-zA-Z1-9\-_\/]+"})
     *
     * @param Categorie $categorie La catégorie récupérée
     * @param PaintRepository $pr pour récupérer les peintures
     * @return Response
     */
    public function categorie(Categorie $categorie, PaintRepository $pr): Response
    {
        $paints = $pr->findByCategory($categorie);

        dump($paints);
        return $this->render('paint/categorie.html.twig', [
            'paints' => $paints,
            'categorie' => $categorie,
        ]);
    }

    /**
     * Détails d'une peinture
     * @Route("/realisations/{slug}",
     * name="app_realisations_details",
     * methods={"GET"},
     * requirements={"slug"="[a-zA-Z1-9\-_\/]+"})
     *
     * @param Paint $paint peinture récupérer via le slug
     * @return Response
     */
    public function details(Paint $paint): Response
    {
        return $this->render('paint/details.html.twig', [
            'paint' => $paint,
        ]);
    }
}
