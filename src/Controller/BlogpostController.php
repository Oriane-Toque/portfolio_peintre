<?php

namespace App\Controller;

use App\Entity\Blogpost;
use App\Repository\BlogpostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogpostController extends AbstractController
{
    /**
     * Liste toutes les actualités du peintre
     * @Route("/actualites", name="app_actualites", methods={"GET"})
     *
     * @param BlogpostRepository $br pour récupérer les actualités
     * @param PaginatorInterface $paginator configuration de la pagination
     * @param Request $request pour récupérer le numéro de la page
     * @return Response
     */
    public function blogposts(
        BlogpostRepository $br,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $br->findAll();

        $blogposts = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );

        // Je configure le rendu de la barre pagination
        $blogposts->setCustomParameters([
            'align' => 'center',
            'size' => 'large',
            'rounded' => true,
        ]);

        return $this->render('blogpost/blogposts.html.twig', [
            'blogposts' => $blogposts,
        ]);
    }

    /**
     * Page détails d'une actualité
     * @Route("/actualites/{slug}",
     * name="app_actualites_details",
     * methods={"GET"},
     * requirements={"slug"="[a-zA-Z1-9\-_\/]+"})
     *
     * @param Blogpost $blogpost actualité récupérée via le slug
     * @return Response
     */
    public function details(Blogpost $blogpost): Response
    {
        return $this->render('blogpost/details.html.twig', [
            'blogpost' => $blogpost,
        ]);
    }
}
