<?php

namespace App\Controller;

use App\Repository\BlogpostRepository;
use App\Repository\PaintRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * Page d'accueil
     * @Route("/", name="app_home", methods={"GET"})
     *
     * @param PaintRepository $pr pour récupérer les dernières peintures
     * @param BlogpostRepository $br pour récupérer les dernières actualités
     * @return Response
     */
    public function home(PaintRepository $pr, BlogpostRepository $br): Response
    {
        // Récupération des 3 dernières peintures
        $lastPaint = $pr->findLast(3);
        // Récupération des 3 derniers blogposts
        $lastBlogpost = $br->findLast(3);

        return $this->render('home/index.html.twig', [
            'paints' => $lastPaint,
            'blogposts' => $lastBlogpost,
        ]);
    }

    /**
     * Page à propos du peintre
     * @Route("/a-propos", name="app_about", methods={"GET"})
     *
     * @param UserRepository $ur pour récupérer l'utilisateur peintre
     * @return Response
     */
    public function about(UserRepository $ur): Response
    {
        $painter = $ur->getPainter();

        return $this->render('about/about.html.twig', [
            'painter' => $painter,
        ]);
    }
}
