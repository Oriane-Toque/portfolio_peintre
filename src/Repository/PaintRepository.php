<?php

namespace App\Repository;

use App\Entity\Categorie;
use App\Entity\Paint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Paint|null find($id, $lockMode = null, $lockVersion = null)
 * @method Paint|null findOneBy(array $criteria, array $orderBy = null)
 * @method Paint[]    findAll()
 * @method Paint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaintRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Paint::class);
    }

    // ======================================================================
    // ======================== CUSTOM REQUEST ==============================

    /**
     * Récupère les dernières peintures publiées
     *
     * @param int $max
     * @return Paint[]|mixed
     */
    public function findLast(int $max)
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.id', 'DESC')
            ->setMaxResults($max)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Récupère toutes les peintures selon la catégorie
     * et qui doivent être affiché dans le portfolio
     *
     * @param Categorie $category
     * @return Paint[]|mixed
     */
    public function findByCategory(Categorie $category)
    {
        return $this->createQueryBuilder('p')
            ->where(':category MEMBER OF p.categorie')
            ->andWhere('p.portfolio = true')
            ->setParameter('category', $category)
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Paint
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
