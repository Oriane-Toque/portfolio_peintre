<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use App\Repository\CategorieRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class AppExtension extends AbstractExtension
{
    private CategorieRepository $cr;
    private RouterInterface $router;

    /**
     * @param CategorieRepository $cr CategoryRepository
     */
    public function __construct(CategorieRepository $cr, RouterInterface $router)
    {
        $this->cr = $cr;
        $this->router = $router;
    }

    /**
     * Renvoie un tableau de fonctions ou filtres
     *
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('categorieNav', [$this, 'getCategorie']),
            new TwigFunction('pageReferer', [$this, 'getReferer']),
        ];
    }

    /**
     * Retourne toutes les catégories (nom & slug)
     *
     * @return Categorie[]|mixed
     */
    public function getCategorie()
    {
        return $this->cr->findAllMenu();
    }

    /**
     * Retourne la route présente dans le headers
     *
     * @param Request $request
     * @return string
     */
    public function getReferer(Request $request)
    {
        $referer = $request->headers->get('referer');

        if (strval($referer) === '') {
            return 'app_actualites';
        }

        $refererPathInfo = Request::create(strval($referer))->getPathInfo();
        $routeInfos = $this->router->match($refererPathInfo);

        $refererRoute = $routeInfos['_route'] ?? 'app_actualites';

        return $refererRoute;
    }
}
