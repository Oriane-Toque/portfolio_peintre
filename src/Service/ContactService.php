<?php

namespace App\Service;

use App\Entity\Contact;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ContactService
{
    private EntityManagerInterface $em;
    private FlashBagInterface $flash;

    /**
     * @param EntityManagerInterface $em
     * @param FlashBagInterface $flash
     */
    public function __construct(EntityManagerInterface $em, FlashBagInterface $flash)
    {
        $this->em = $em;
        $this->flash = $flash;
    }

    /**
     * Attribue les dernières données à l'objet puis la persiste
     *
     * @param Contact $contact
     * @return void
     */
    public function persistContact(Contact $contact)
    {
        $contact->setCreatedAt(new DateTime('now'))
                ->setIsSend(false);

        $this->em->persist($contact);
        $this->em->flush();

        $this->flash->add(
            'success',
            'Votre message a bien été envoyé, vous recevrez une réponse dans les plus brefs délais !'
        );
    }

    /**
     * Indique si la prise de contact a été envoyé par mail
     *
     * @param Contact $contact
     * @return void
     */
    public function isSend(Contact $contact)
    {
        $contact->setIsSend(true);
        $this->em->persist($contact);
        $this->em->flush();
    }
}
