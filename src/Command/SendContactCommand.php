<?php

namespace App\Command;

use App\Repository\ContactRepository;
use App\Repository\UserRepository;
use App\Service\ContactService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class SendContactCommand extends Command
{
    private ContactRepository $cr;
    private MailerInterface $mailer;
    private ContactService $cs;
    private UserRepository $ur;
    protected static $defaultName = 'app:send:contact';

    public function __construct(
        ContactRepository $cr,
        MailerInterface $mailer,
        ContactService $cs,
        UserRepository $ur
    ) {
        $this->cr = $cr;
        $this->mailer = $mailer;
        $this->cs = $cs;
        $this->ur = $ur;
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $toSend = $this->cr->findBy(['isSend' => false]);
        $painter = $this->ur->getPainter();

        $address = new Address($painter->getEmail(), $painter);

        foreach ($toSend as $mail) {
            $email = (new TemplatedEmail())
                ->from($mail->getEmail())
                ->to($address)
                ->subject('Painter portfolio : ' . $mail->getName())
                ->htmlTemplate('contact/mail.html.twig')
                ->context([
                    'contactName' => $mail->getName(),
                    'contactEmail' => $mail->getEmail(),
                    'message' => $mail->getMessage(),
                    'createdAt' => $mail->getCreatedAt(),
                ])
            ;

            $this->mailer->send($email);

            $this->cs->isSend($mail);
        }
        return Command::SUCCESS;
    }
}
