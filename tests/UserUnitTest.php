<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Paint;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    /**
     * @return void
     */
    public function testIsTrue()
    {
        $user = new User;

        $user->setEmail('true@test.com')
             ->setFirstname('firstname')
             ->setLastname('lastname')
             ->setPassword('password')
             ->setAbout('about')
             ->setTel('tel')
             ->setRoles(['USER'])
             ->setInstagram('instagram');
        
        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getUsername() === 'true@test.com');
        $this->assertTrue($user->getUserIdentifier() === 'true@test.com');
        $this->assertTrue($user->getFirstname() === 'firstname');
        $this->assertTrue($user->getLastname() === 'lastname');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getRoles() === ['USER', 'ROLE_USER']);
        $this->assertTrue($user->getAbout() === 'about');
        $this->assertTrue($user->getTel() === 'tel');
        $this->assertTrue($user->getInstagram() === 'instagram');
    }
    /**
     * @return void
     */
    public function testIsFalse()
    {
        $user = new User;

        $user->setEmail('true@test.com')
             ->setFirstname('firstname')
             ->setLastname('lastname')
             ->setPassword('password')
             ->setAbout('about')
             ->setRoles(['USER'])
             ->setTel('tel')
             ->setInstagram('instagram');
        
        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getUsername() === 'false@test.com');
        $this->assertFalse($user->getUserIdentifier() === 'false@test.com');
        $this->assertFalse($user->getFirstname() === 'false');
        $this->assertFalse($user->getLastname() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getAbout() === 'false');
        $this->assertFalse($user->getRoles() === ['false']);
        $this->assertFalse($user->getTel() === 'false');
        $this->assertFalse($user->getInstagram() === 'false');
    }
    /**
     * @return void
     */
    public function testIsEmpty()
    {
        $user = new User;

        $user->setPassword('');
        $user->setEmail('');
        
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getUsername());
        $this->assertEmpty($user->getUserIdentifier());
        $this->assertEmpty($user->getFirstname());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAbout());
        $this->assertEmpty($user->getInstagram());
        $this->assertEmpty($user->getId());
        $this->assertEmpty($user->getTel());
    }
    /**
     * @return void
     */
    public function testIsToString()
    {
        $user = new User;

        $user->setFirstname('toto')
             ->setLastname('titi');

        $this->assertTrue($user == 'toto titi');
    }
    /**
     * @return void
     */
    public function testAddGetRemovePaintBlogpost()
    {
        $user = new User;
        $paint = new Paint;
        $blogpost = new Blogpost;

        // Je vérifie si j'obtiens une peinture (vide)
        $this->assertEmpty($user->getPaints());
        // Je vérifie si j'obtiens un blogpost (vide)
        $this->assertEmpty($user->getBlogposts());

        // Je vérifie si je peux ajouter une peinture
        $user->addPaint($paint);
        $this->assertContains($paint, $user->getPaints());
        // Je vérifie si je peux ajouter un blogpost
        $user->addBlogpost($blogpost);
        $this->assertContains($blogpost, $user->getBlogposts());

        // Je vérifie si je peux supprimer une peinture
        $user->removePaint($paint);
        $this->assertEmpty($user->getPaints());
        // Je vérifie si je peux supprimer un blogpost
        $user->removeBlogpost($blogpost);
        $this->assertEmpty($user->getBlogposts());
    }
}
