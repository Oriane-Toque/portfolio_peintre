<?php

namespace App\Tests;

use App\Entity\Contact;
use DateTime;
use PHPUnit\Framework\TestCase;

class ContactUnitTest extends TestCase
{
    /**
     * @return void
     */
    public function testIsTrue()
    {
        $contact = new Contact;
        $datetime = new DateTime();

        $contact->setName('Fabien')
                ->setEmail('fabien@gmail.com')
                ->setMessage('message')
                ->setIsSend(true)
                ->setCreatedAt($datetime);

        $this->assertTrue($contact->getName() === 'Fabien');
        $this->assertTrue($contact->getEmail() === 'fabien@gmail.com');
        $this->assertTrue($contact->getMessage() === 'message');
        $this->assertTrue($contact->getIsSend() === true);
        $this->assertTrue($contact->getCreatedAt() === $datetime);
    }
    /**
     * @return void
     */
    public function testIsFalse()
    {
        $contact = new Contact;
        $datetime = new DateTime();

        $contact->setName('Fabien')
                ->setEmail('fabien@gmail.com')
                ->setMessage('message')
                ->setIsSend(true)
                ->setCreatedAt($datetime);

        $this->assertFalse($contact->getName() === 'false');
        $this->assertFalse($contact->getEmail() === 'false');
        $this->assertFalse($contact->getMessage() === 'false');
        $this->assertFalse($contact->getIsSend() === false);
        $this->assertFalse($contact->getCreatedAt() === new DateTime());
    }
    /**
     * @return void
     */
    public function testIsEmpty()
    {
        $contact = new Contact;

        $this->assertEmpty($contact->getName());
        $this->assertEmpty($contact->getEmail());
        $this->assertEmpty($contact->getMessage());
        $this->assertEmpty($contact->getIsSend());
        $this->assertEmpty($contact->getCreatedAt());
        $this->assertEmpty($contact->getId());
    }
}
