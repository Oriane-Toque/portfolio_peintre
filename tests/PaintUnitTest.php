<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\Comment;
use App\Entity\Paint;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class PaintUnitTest extends TestCase
{
    /**
     * @return void
     */
    public function testIsTrue()
    {
        $paint = new Paint;
        $datetime = new DateTime();
        $categorie = new Categorie;
        $user = new User;

        $paint->setName('nom')
              ->setWidth(20.20)
              ->setHeight(20.20)
              ->setOnSale(true)
              ->setPrice(20.20)
              ->setRealisationDate($datetime)
              ->setCreatedAt($datetime)
              ->setDescription('description')
              ->setPortfolio(true)
              ->setSlug('slug')
              ->setFile('file')
              ->addCategorie($categorie)
              ->setUser($user);

        $this->assertTrue($paint->getName() === 'nom');
        $this->assertTrue($paint->getWidth() == 20.20);
        $this->assertTrue($paint->getHeight() == 20.20);
        $this->assertTrue($paint->getPrice() == 20.20);
        $this->assertTrue($paint->getOnSale() === true);
        $this->assertTrue($paint->getRealisationDate() === $datetime);
        $this->assertTrue($paint->getCreatedAt() === $datetime);
        $this->assertTrue($paint->getDescription() === 'description');
        $this->assertTrue($paint->getPortfolio() === true);
        $this->assertTrue($paint->getSlug() === 'slug');
        $this->assertTrue($paint->getFile() === 'file');
        $this->assertTrue($paint->getUser() === $user);
        $this->assertContains($categorie, $paint->getCategorie());
    }
    /**
     * @return void
     */
    public function testIsFalse()
    {
        $paint = new Paint;
        $datetime = new DateTime();
        $categorie = new Categorie;
        $user = new User;

        $paint->setName('nom')
              ->setWidth(20.20)
              ->setHeight(20.20)
              ->setOnSale(true)
              ->setPrice(20.20)
              ->setRealisationDate($datetime)
              ->setCreatedAt($datetime)
              ->setDescription('description')
              ->setPortfolio(true)
              ->setSlug('slug')
              ->setFile('file')
              ->addCategorie($categorie)
              ->setUser($user);

        $this->assertFalse($paint->getName() === 'false');
        $this->assertFalse($paint->getWidth() == 22.22);
        $this->assertFalse($paint->getHeight() == 22.22);
        $this->assertFalse($paint->getPrice() == 22.22);
        $this->assertFalse($paint->getOnSale() === false);
        $this->assertFalse($paint->getRealisationDate() === new DateTime());
        $this->assertFalse($paint->getCreatedAt() === new DateTime());
        $this->assertFalse($paint->getDescription() === 'false');
        $this->assertFalse($paint->getPortfolio() === false);
        $this->assertFalse($paint->getSlug() === 'false');
        $this->assertFalse($paint->getFile() === 'false');
        $this->assertFalse($paint->getUser() === new User);
        $this->assertNotContains(new Categorie, $paint->getCategorie());
    }
    /**
     * @return void
     */
    public function testIsEmpty()
    {
        $paint = new Paint;

        $paint->setFile('');

        $this->assertEmpty($paint->getName());
        $this->assertEmpty($paint->getWidth());
        $this->assertEmpty($paint->getHeight());
        $this->assertEmpty($paint->getPrice());
        $this->assertEmpty($paint->getOnSale());
        $this->assertEmpty($paint->getRealisationDate());
        $this->assertEmpty($paint->getCreatedAt());
        $this->assertEmpty($paint->getDescription());
        $this->assertEmpty($paint->getPortfolio());
        $this->assertEmpty($paint->getSlug());
        $this->assertEmpty($paint->getFile());
        $this->assertEmpty($paint->getUser());
        $this->assertEmpty($paint->getCategorie());
        $this->assertEmpty($paint->getId());
    }
    /**
     * @return void
     */
    public function testAddGetRemoveComments()
    {
        $paint = new Paint;
        $comment = new Comment;

        // Je vérifie si j'obtiens un commentaire (vide)
        $this->assertEmpty($paint->getComments());

        // Je vérifie si je peux ajouter un commentaire
        $paint->addComment($comment);
        $this->assertContains($comment, $paint->getComments());

        // Je vérifie si je peux supprimer un commentaire
        $paint->removeComment($comment);
        $this->assertEmpty($paint->getComments());
    }
}
