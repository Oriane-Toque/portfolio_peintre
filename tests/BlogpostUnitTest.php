<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Comment;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class BlogpostUnitTest extends TestCase
{
    /**
     * @return void
     */
    public function testIsTrue()
    {
        $blogpost = new Blogpost;
        $datetime = new DateTime();
        $user = new User;

        $blogpost->setTitle('titre')
                 ->setContent('content')
                 ->setSlug('slug')
                 ->setCreatedAt($datetime)
                 ->setUser($user);

        $this->assertTrue($blogpost->getTitle() === 'titre');
        $this->assertTrue($blogpost->getContent() === 'content');
        $this->assertTrue($blogpost->getSlug() === 'slug');
        $this->assertTrue($blogpost->getCreatedAt() === $datetime);
        $this->assertTrue($blogpost->getUser() === $user);
    }
    /**
     * @return void
     */
    public function testIsFalse()
    {
        $blogpost = new Blogpost;
        $datetime = new DateTime();
        $user = new User;

        $blogpost->setTitle('titre')
                 ->setContent('content')
                 ->setSlug('slug')
                 ->setCreatedAt($datetime)
                 ->setUser($user);

        $this->assertFalse($blogpost->getTitle() === 'false');
        $this->assertFalse($blogpost->getContent() === 'false');
        $this->assertFalse($blogpost->getSlug() === 'false');
        $this->assertFalse($blogpost->getCreatedAt() === new DateTime());
        $this->assertFalse($blogpost->getUser() === new User);
    }
    /**
     * @return void
     */
    public function testIsEmpty()
    {
        $blogpost = new Blogpost;

        $this->assertEmpty($blogpost->getTitle());
        $this->assertEmpty($blogpost->getContent());
        $this->assertEmpty($blogpost->getSlug());
        $this->assertEmpty($blogpost->getCreatedAt());
        $this->assertEmpty($blogpost->getUser());
        $this->assertEmpty($blogpost->getId());
    }
    /**
     * @return void
     */
    public function testAddGetRemoveComment()
    {
        $blogpost = new Blogpost;
        $comment = new Comment;

        // Je vérifie si j'obtiens un commentaire (vide)
        $this->assertEmpty($blogpost->getComments());

        // Je vérifie si je peux ajouter un commentaire
        $blogpost->addComment($comment);
        $this->assertContains($comment, $blogpost->getComments());

        // Je vérifie si je peux supprimer un commentaire
        $blogpost->removeComment($comment);
        $this->assertEmpty($blogpost->getComments());
    }
}
