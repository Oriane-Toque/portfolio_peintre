<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\Paint;
use PHPUnit\Framework\TestCase;

class CategorieUnitTest extends TestCase
{
    /**
     * @return void
     */
    public function testIsTrue()
    {
        $categorie = new Categorie;

        $categorie->setName('nom')
                  ->setDescription('description')
                  ->setSlug('slug');

        $this->assertTrue($categorie->getName() === 'nom');
        $this->assertTrue($categorie->getDescription() === 'description');
        $this->assertTrue($categorie->getSlug() === 'slug');
    }
    /**
     * @return void
     */
    public function testIsFalse()
    {
        $categorie = new Categorie;

        $categorie->setName('nom')
                  ->setDescription('description')
                  ->setSlug('slug');

        $this->assertFalse($categorie->getName() === 'false');
        $this->assertFalse($categorie->getDescription() === 'false');
        $this->assertFalse($categorie->getSlug() === 'false');
    }
    /**
     * @return void
     */
    public function testIsEmpty()
    {
        $categorie = new Categorie;

        $this->assertEmpty($categorie->getName());
        $this->assertEmpty($categorie->getDescription());
        $this->assertEmpty($categorie->getSlug());
        $this->assertEmpty($categorie->getId());
    }
    /**
     * @return void
     */
    public function testIsToString()
    {
        $categorie = new Categorie;

        $categorie->setName('toto');

        $this->assertTrue($categorie == 'toto');
    }
    /**
     * @return void
     */
    public function testAddGetRemovePaint()
    {
        $categorie = new Categorie;
        $paint = new Paint;

        // Je vérifie si j'obtiens une peinture (vide)
        $this->assertEmpty($categorie->getPaints());

        // Je vérifie si je peux ajouter une peinture
        $categorie->addPaint($paint);
        $this->assertContains($paint, $categorie->getPaints());

        // Je vérifie si je peux supprimer une peinture
        $categorie->removePaint($paint);
        $this->assertEmpty($categorie->getPaints());
    }
}
