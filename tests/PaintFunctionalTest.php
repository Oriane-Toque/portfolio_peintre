<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PaintFunctionalTest extends WebTestCase
{
    public function testShouldDisplayRealisations(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/realisations');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Réalisations');
    }

    public function testShouldDisplayPortfolioCategory(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/portfolio');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Portfolio');
    }

    public function testShouldDisplayPortfolio(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/portfolio/category-test');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Category test');
    }

    public function testShouldDisplayOneRealisation(): void
    {
        $client = static:: createClient();
        $crawler = $client->request('GET', '/realisations/paint-test');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Paint test');
    }
}
