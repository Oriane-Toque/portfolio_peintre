<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Comment;
use App\Entity\Paint;
use DateTime;
use PHPUnit\Framework\TestCase;

class CommentUnitTest extends TestCase
{
    /**
     * @return void
     */
    public function testIsTrue()
    {
        $comment = new Comment;
        $datetime = new DateTime();
        $paint = new Paint;
        $blogpost = new Blogpost;

        $comment->setAuthor('auteur')
                ->setEmail('email')
                ->setContent('content')
                ->setCreatedAt($datetime)
                ->setPaint($paint)
                ->setBlogpost($blogpost);

        $this->assertTrue($comment->getAuthor() === 'auteur');
        $this->assertTrue($comment->getEmail() === 'email');
        $this->assertTrue($comment->getContent() === 'content');
        $this->assertTrue($comment->getCreatedAt() === $datetime);
        $this->assertTrue($comment->getPaint() === $paint);
        $this->assertTrue($comment->getBlogpost() === $blogpost);
    }
    /**
     * @return void
     */
    public function testIsFalse()
    {
        $comment = new Comment;
        $datetime = new DateTime();
        $paint = new Paint;
        $blogpost = new Blogpost;

        $comment->setAuthor('auteur')
                ->setEmail('email')
                ->setContent('content')
                ->setCreatedAt($datetime)
                ->setPaint($paint)
                ->setBlogpost($blogpost);

        $this->assertFalse($comment->getAuthor() === 'false');
        $this->assertFalse($comment->getEmail() === 'false');
        $this->assertFalse($comment->getContent() === 'false');
        $this->assertFalse($comment->getCreatedAt() === new DateTime());
        $this->assertFalse($comment->getPaint() === new Paint);
        $this->assertFalse($comment->getBlogpost() === new Blogpost);
    }
    /**
     * @return void
     */
    public function testIsEmpty()
    {
        $comment = new Comment;

        $this->assertEmpty($comment->getAuthor());
        $this->assertEmpty($comment->getEmail());
        $this->assertEmpty($comment->getContent());
        $this->assertEmpty($comment->getCreatedAt());
        $this->assertEmpty($comment->getPaint());
        $this->assertEmpty($comment->getBlogpost());
        $this->assertEmpty($comment->getId());
    }
}
